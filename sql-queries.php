<?php

// Connect to the database using MySQLi
$host = 'localhost:3307';
$username = 'root';
$password = '';
$database = 'employeedb';


$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Insert data into the employee table
    /*  The code stores a SQL query to insert data into the "employee" table. 
        It specifies the columns (first_name, last_name, middle_name, birthday, and address) 
        and their corresponding values for a new record to be added. */
$sql = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address)
            VALUES ('Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street');";
if ($conn->query($sql) === TRUE) {
    echo "New record created successfully <br>";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

// retrieve the first name, last name, and birthday of all employees in the table
    /* This code retrieves the first name, last name, and birthday of all employees from the "employee" table.*/
$sql =  "SELECT first_name, last_name, birthday FROM employee";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<br>" . "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
    }
} else {
    echo "0 results";
}

// retrieve the number of employees whose last name starts with the letter 'S'
    /*  This code retrieves the count of employees whose last name starts with the letter 'S' from the "employee" table. 
        It uses the SQL query SELECT COUNT(*) AS count FROM employee WHERE last_name LIKE 'S%'; */
$sql = "SELECT COUNT(*) AS count FROM employee WHERE last_name LIKE 'D%'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $count = $row["count"];
    echo "<br> Number of employees whose last name starts with the letter 'D': " . $count;
} else {
    echo "0 results <br> ";
}

//  retrieve the first name, last name, and address of the employee with the highest ID number
    /*  This code retrieves the first name, last name, and address of the employee with the highest 
        ID number from the "employee" table. It uses the SQL query SELECT first_name, last_name, address 
        FROM employee WHERE id = (SELECT MAX(id) FROM employee) */
$sql = "SELECT first_name, last_name, address FROM employee WHERE id = (SELECT MAX(id) FROM employee)";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<br>" . "<br> First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Address: " . $row["address"]. "<br>";
    }
} else {
    echo "<br> 0 results ";
}

// Update data in the employee table
    /*  This code updates the "address" column of a record in the "employee" table. 
        It sets the address to '123 Main Street' for the employee with the first name 'Jerry'.*/
$sql = "UPDATE employee SET address = '123 Main Street' WHERE first_name = 'Jerry'";
if ($conn->query($sql) === TRUE) {
    echo "<br> Record updated successfully <br>";
} else {
    echo "Error updating record: " . $conn->error;
}

// Delete data from the employee table
    /*  This code deletes records from the "employee" table where the "last_name" column starts with 'D'. 
        It uses the SQL query DELETE FROM employee WHERE last_name LIKE 'D%'; string value*/
$sql = "DELETE FROM employee WHERE last_name LIKE 'D%';";
if ($conn->query($sql) === TRUE) {
    echo "<br> Record deleted successfully";
} else {
    echo "Error deleting record: " . $conn->error;
}

$conn->close();

?>